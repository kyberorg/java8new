package ws.kyberorg;

import java.util.Arrays;
import java.util.Comparator;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class Main {

    public static void main(String[] args) {
        lambda();
    }

    public static void lambda(){
        //lambdas
        BinaryOperator<Integer> subtract = (x,y) -> x -y;
        int r = subtract.apply(8, 3);
        String rslt;
        if(r==5){
            rslt = "OK";
        } else {
            rslt = "fail";
        }
        System.out.println(rslt);

        //in: int,int
        //out: boolean
        Comparator<Integer> comparator = (a,b) -> {
            int x = a;
            int y = b;
            return (x < y) ? -1 :
                    (x==y) ? 0 : 1;
        };

        comparator.compare(4,3);

        //in void
        //out int
        Supplier<Integer> life = () -> {
            return 42;
        };

        life.get();

        //Consumer<T> - void accept (T t);

        Consumer<String> c = s -> {
            System.out.println(s); };

        Arrays.asList("One", "Two" ).forEach(c);
    }
}
